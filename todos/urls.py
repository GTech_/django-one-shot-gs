from django.urls import path

from todos.views import (
    TodoListListView,
    TodoListDetailView,
    TodoListCreateView,
    TodoItemCreateView,
    TodoListUpdateView,
    TodoListDeleteView,
    TodoItemUpdateView,

)


urlpatterns = [
    path('', TodoListListView.as_view(), name = "todolist_list"),
    path('create/', TodoListCreateView.as_view(), name = "todolist_new"),
    path('<int:pk>/', TodoListDetailView.as_view(), name = "todolist_detail"),
    path('<int:pk>/edit/', TodoListUpdateView.as_view(), name = "todolist_edit"),
    path('<int:pk>/delete/', TodoListDeleteView.as_view(), name = "todolist_delete"),
    path('items/create/', TodoItemCreateView.as_view(), name = "todoitem_new"),
    path('items/<int:pk>/edit/', TodoItemUpdateView.as_view(), name = "todoitem_edit"),
]