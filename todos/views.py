from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.urls import reverse_lazy
from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = 'todos/list.html'


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = 'todos/detail.html'


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = 'todos/new.html'
    fields = ['name']
    success_url = reverse_lazy('todolist_list')

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = 'todos/edit.html'
    fields = ['name']
    success_url = reverse_lazy('todolist_list')

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = 'todos/delete.html'
    success_url = reverse_lazy('todolist_list')


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = 'todos/newitem.html'
    fields = ['task', 'due_date', 'is_completed', 'list']
    success_url = reverse_lazy('todolist_list')


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = 'todos/edititem.html'
    fields = ['task', 'due_date', 'is_completed', 'list']
    success_url = reverse_lazy('todolist_list')